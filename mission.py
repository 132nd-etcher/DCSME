﻿import mizfile, os.path
from slpp import SLPP

class openMission():

    __level1_checks = (
                'usedModules','groundControl','descriptionBlueTask',
                'start_time','pictureFileNameB','currentKey','trigrules',
                'sortie','coalitions','descriptionText','resourceCounter',
                'theatre','needModules','map','forcedOptions','failures',
                'result','triggers','goals','version','pictureFileNameR',
                'descriptionRedTask','weather','coalition','trig'
                )

    def __init__(self, path_to_miz_file, temp_dir=None):
        self.path_to_miz_file = path_to_miz_file
        self.temp_dir = temp_dir

    def __enter__(self):
        self.mizfile = mizfile.MizFile(self.path_to_miz_file).check().decompact()
        self.path_to_mission_file = os.path.join(self.mizfile.temp_dir,"mission")
        parser = SLPP()
        try:
            with open(self.path_to_mission_file,mode="rb") as f:
                self.raw_text = f.readlines()
        except:
            Exception("Impossible de lire le fichier mission suivant: {}".format(self.path_to_mission_file))
        self.d = parser.decode("\n".join(self.raw_text[1:]))
        self.check()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            print("DEBUG: exception occured: \n\tType: {}\n\Value:\n\tTraceback: {}".format(exc_type, exc_value, exc_traceback))
        self.mizfile.delete_temp_dir()

    def write(self):
        parser = SLPP()
        raw_text = parser.encode(self.d)
        with open(os.path.abspath(os.path.join(self.mizfile.temp_dir,"mission")), mode="wb") as out_file:
            out_file.write('mission = ')
            out_file.write(raw_text)
        self.mizfile.recompact()

    def short_summary(self):
        """
        Retourne une string avec un mini-résumé de la table de mission

        Présente sous forme indentée les deux premiers niveaux de la table LUA
        du fichier mission
        """
        return '\n'.join(['{}: {}'.format(k, self.d[k]) for k in self.d.keys()])

    def check(self):
        """
        Compare la table de mission à une liste prédeterminée, pour vérifier la
        présence de toutesles informations nécessaires
        """
        self.__check_dict(self.d, openMission.__level1_checks)

    def __check_dict(self,d,proof):
        """
        Vérifies que le dictionnaire "d" possède toutes les clefs reprises dans "proof"
        """
        d_keys = d.keys()
        try:
            for p in proof:
                if not p in d_keys:
                    raise KeyError
        except KeyError:
            raise Exception("Impossible de trouver la clef \"{}\" dans la table de mission: {}"
            .format(p, os.path.dirname(self.path_to_mission_file)))


