if __name__ == '__main__':

    import sys
    import mission
    with mission.openMission('test.miz') as m:
        print(m.short_summary())
        m.write()
    sys.exit()