﻿import string, random, os


def random_string(prefix="", suffix="", size=8, chars=string.ascii_uppercase + string.digits):
    random_bit =  ''.join(random.choice(chars) for x in range(size))
##   return random_bit
    return "{}{}{}".format(prefix,random_bit,suffix)

def random_folder(base_path, prefix="", suffix="", size=8, chars=string.ascii_uppercase + string.digits):
    return os.path.join(base_path, random_string(prefix, suffix, size, chars))
